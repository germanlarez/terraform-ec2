# Terraform-EC2

## If never worked with terraform use this [repo](https://gitlab.com/germanlarez/terraform.git) for reference.

## Deploys an EC2 instance and deploys a docker container in it.

![Terraform-ec2](images/terraform-EC2.png)

### Creates a custom VPC.

**Know you AZ availables for your selected regions, in this case** `us-east-1`

`$ aws ec2 describe-availability-zones --region us-east-1 --output text --query AvailabilityZones[*].ZoneName[]`

Select 1 ZoneName for you subnet, IE `us-east-1a`.

Declare variables for:

- Vpc and subnet cidr blocks.
- Availability zones for your subnet.
- Environment prefix tag for your resources.
- your IP to allow ssh connection later.
- Instance Type.
- SSH Key Pair location to connect with your EC2.

`terraform.tfvars`

    vpc_cidr_block = "10.0.0.0/16"
    subnet_cidr_block = "10.0.10.0/24"
    avail_zone = "us-east-1a"
    env_prefix = "dev"
    my_ip = ["186.104.242.86/32"]
    instance_type = "t2.micro"
    public_key_location = "~/.ssh/id_ed25519.pub"

`main.tf`

        resource "aws_vpc" "myapp-vpc" {
            cidr_block = var.vpc_cidr_block
            tags = {
                "Name" = "${var.env_prefix}-vpc"
                }
            }
___

### Creates a subnet in 1 AZ and associate it to a VPC.
___

`main.tf`

        resource "aws_subnet" "myapp-priv-subnet-1" {
            vpc_id = aws_vpc.myapp-vpc.id
            cidr_block = var.subnet_cidr_block
            availability_zone = var.avail_zone
            tags = {
                "Name" = "${var.env_prefix}-subnet-1"
                }
            }

### Creates an Internet Gateway, a Route Table and connect it to the VPC.
___

`$ main.tf`

        resource "aws_internet_gateway" "myapp-igw" {
            vpc_id = aws_vpc.myapp-vpc.id
            tags = {
                "Name" = "${var.env_prefix}-igw"
                } 
            }

        resource "aws_route_table" "myapp-route-table" {
            vpc_id = aws_vpc.myapp-vpc.id
            route {
                cidr_block = "0.0.0.0/0"
                gateway_id = aws_internet_gateway.myapp-igw.id
                }
            tags = {
                "Name" = "${var.env_prefix}-rtb"
                } 
            }

        resource "aws_route_table_association" "a-rtb-subnet-myapp" {
            subnet_id = aws_subnet.myapp-priv-subnet-1.id
            route_table_id = aws_route_table.myapp-route-table.id  
        }

### Creates a Security Group for SSH and app access.
___

`$ main.tf`

        resource "aws_security_group" "myapp-sg" {
            name = "myapp-sg"
            vpc_id = aws_vpc.myapp-vpc.id
        
        ingress {
            from_port = 22
            to_port = 22
            protocol = "tcp"
            cidr_blocks = var.my_ip
            description = "SSH Access control"
            }

        ingress {
            from_port = 80
            to_port = 80
            protocol = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
            description = "myapp http Access control"
            }
        
        egress {
            from_port        = 0
            to_port          = 0
            protocol         = "-1"
            cidr_blocks      = ["0.0.0.0/0"]
            ipv6_cidr_blocks = ["::/0"]
            } 
        }

### Provision an EC2 Instance.
___

`$ main.tf`

**Define your AMI as data so you get the latest from AWS Linux in this case:**

    data "aws_ami" "latest-amazon-linux-ami" {
        most_recent = true
        owners = [ "amazon" ]
        filter {
            name = "name"
            values = ["amzn2-ami-hvm-*-gp2"]
            }
        filter {
            name = "architecture"
            values = ["x86_64"]
            }
        filter {
            name = "virtualization-type"
            values = ["hvm"]
            }
        filter {
            name = "root-device-type"
            values = ["ebs"]
            }
        }

You can get more examples for other owners in official [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) or if you have your own AMI.

**Define your Instance:**

`$ main.tf`

    resource "aws_instance" "myapp-server" {
        ami = data.aws_ami.latest-amazon-linux-ami.id
        instance_type = var.instance_type
        subnet_id = aws_subnet.myapp-priv-subnet-1.id
        vpc_security_group_ids = [aws_security_group.myapp-sg.id]
        availability_zone = var.avail_zone
        associate_public_ip_address = true
        key_name = "terraform-ec2"
    
        tags = {
            Name = "${var.env_prefix}-myapp-server"
        }

**Create a KeyPair so you can login later:**

`$ main.tf`

    resource "aws_key_pair" "ssh-key" {
        key_name = "${var.env_prefix}-myapp-server"
        public_key = "${file(var.public_key_location)}"
        }

with file() we can set as an env var the absolute path of a key already located on our computer, in our case was `public_key_location = "~/.ssh/id_ed25519.pub"` on `terraform.tfvars` file.

Now you can connect to it once you apply this configuration.

![ssh-conn](images/ssh-conn.png)


### Deploys a Nginx Docker Container.

**We can achieve this by running an entreypoint script to start a Docker container under user_data attribute:** 

    resource "aws_instance" "myapp-server" {
        ami = data.aws_ami.latest-amazon-linux-ami.id
        instance_type = var.instance_type
        subnet_id = aws_subnet.myapp-priv-subnet-1.id
        vpc_security_group_ids = [aws_security_group.myapp-sg.id]
        availability_zone = var.avail_zone
        associate_public_ip_address = true
        key_name = aws_key_pair.ssh-key.key_name
        user_data = <<EOF
                        !#/bin/bash
                        sudo yum update -y && sudo yum install -y docker
                        sudo systemctl start docker
                        sudo usermod -aG docker ec2-user
                        docker run -p 8080:80 nginx
                    EOF

        tags = {
            Name = "${var.env_prefix}-server"
        }
    }

___
